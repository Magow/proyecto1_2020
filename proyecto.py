#  !/usr/bin/env python3
#   -*- coding:utf-8 -*-

#   Importar librerías a usar y la función system para borrar pantalla
from os import system
import random
import time
import copy


#   Impresión de matriz para las interacciones y material visual
def imprimir(matriz, tam):

    for i in range(tam):
        #  Casilla = espacio que contiene al valor de cada posición,
        #  se reinicia luego de cada interacción
        casilla = ''
        for j in range(tam):
            #   Acumulación de las columnas de cada fila con el valor [i][j]
            if matriz[i][j] == "2":
                casilla += "x "
            if matriz[i][j] == "1":
                casilla += ". "
            if matriz[i][j] == "0":
                casilla += "# "
            else:
                continue
        print(casilla)
    print('\n')

#   Solo interactuar con las letras permitidas (WASD)


def excepcion_wasd_string():
        #   Letras permitidas
    wasd_acept = ["W", "A", "S", "D"]
    #   Hasta no ingresar una letra permitida se reinicia la solicitud
    while True:
            string_val = input("Interactúe para realizar un movimiento: ")
            if string_val.upper() in wasd_acept:
                break
            else:
                print("Ha ingresado un valor que no otorga ningún ", end=" ")
                print("tipo de movimiento ¬_¬")
                print("Intente nuevamente")
                continue
    return string_val


#   Revisión de las células permitidas para el movimiento del usuario
def can_u(tablero, tam, posibilidad, eje, i, j):
    #   Comprobar si la interacción es posible con la casilla a la que el
    #   usuario quiere interactuar, por eso el uso de try and except
    limite1 = i + posibilidad
    limite2 = j + posibilidad

    while True:
        try:
            if eje == "y":
                if limite1 < 0 or limite1 > tam - 1:
                    print("Imposible acceder a esta posición, ", end=" ")
                    print("intente nuevamente (choque eje Y)")
                    time.sleep(0.4)
                    break
                else:
                    if matriz[i + posibilidad][j] == "0":
                        print("Imposible acceder a esta posición")
                        print(", intente con espacios permitidos", end=" ")
                        time.sleep(0.4)
                        break
                    if (matriz[i + posibilidad][j] == '1'):
                        tablero[i + posibilidad][j] = "2"
                        tablero[i][j] = "1"
                        break

            if eje == "x":
                if limite2 < 0 or limite2 > tam - 1:
                    print("Imposible acceder a esta posición, ", end=" ")
                    print("intente nuevamente (choque eje X)")
                    time.sleep(0.4)
                    break
                else:
                    if matriz[i][j + posibilidad] == "0":
                        print("Imposible acceder a esta posición, ", end=" ")
                        print("intente con espacios permitidos")
                        time.sleep(0.4)
                        break
                    if (matriz[i][j + posibilidad] == '1'):
                        tablero[i][j + posibilidad] = "2"
                        tablero[i][j] = "1"
                        break

        except IndexError:
            print("Imposible acceder a esta posición, IndexError")

    return tablero


def evaluar_movimiento(tablero, tam, x, y):

    posibilidad = 0
    eje = ""
    wasd_acept = ["W", "A", "S", "D"]
    valor_mov = excepcion_wasd_string()
    #   Entrega de valores y eje de interacción para cada letra
    #   ingresada por el usuario

    if valor_mov.upper() == "W":
        posibilidad -= 1
        eje = "y"

    if valor_mov.upper() == "A":
        posibilidad -= 1
        eje = "x"

    if valor_mov.upper() == "S":
        posibilidad += 1
        eje = "y"

    if valor_mov.upper() == "D":
        posibilidad += 1
        eje = "x"

    return posibilidad, eje


#   Interacciones teclado-matriz para resolver el laberinto
def wasd_mov(matriz, tam, funciona):

    tablero = copy.deepcopy(matriz)
#   Busqueda de la X para empezar a interactuar segun los deseos del usuario
    break_rial = False
    for i in range(tam):
        for j in range(tam):
            #   If de salida por orden en la ejecución de python
            #   Si la X está en la esquina ganadora se cierra el juego
            if (i == (tam-1) and j == (tam-1)) and tablero[tam-1][tam-1] == "2":
                funciona = False
                break
            #   Busqueda de X para interactuar con ella
            if tablero[i][j] == "2":
                #   Imprimir(matriz, tam)
                posibilidad, eje = evaluar_movimiento(tablero, tam, i, j)
                tablero = can_u(tablero, tam, posibilidad, eje, i, j)
                break_rial = True
                break
        if break_rial:
            break

    matriz = copy.deepcopy(tablero)
    system('clear')
    return matriz, funciona


def road_maker(m_inter, tam):
    m_inter_road = copy.deepcopy(m_inter)

    for i in range(tam):
        for j in range(tam):
            posibility = random.randint(0, 1)
            if m_inter[i][j] == "0":
                if i == j or i == j + 1:
                    m_inter_road[i][j] = "1"
                    continue
                if posibility == 1:
                    m_inter_road[i][j] = "1"
                if posibility == 0:
                    m_inter_road[i][j] = "0"
            else:
                continue

    return m_inter_road


def excepcion_tam_int():

    while True:
        try:
            print("**El número debe ser mayor a 10 y menor a 35", end="")
            print (" para una óptima visualización en terminal**")
            int_val = int(input("Ingrese el tamaño de la matriz a formar: "))
            system('clear')
            if int_val > 10 and int_val < 35:

                print("Tamaño de la matriz para simulación = ", int_val, "x", int_val, "\n")
                break
            else:

                print("ingrese un nuevo valor para los parámetros \n")

        except ValueError:
            system('clear')
            print("Ingrese un valor válido de tipo 'int' ¬_¬\n")
            continue

    return int_val


def matriz_inter():
#    creación de matriz
    m_inter = []
    system('clear')
    tam = excepcion_tam_int()
    contador = 0

    for i in range(tam):
        #   Añadir listas para generar cada fila.
        m_inter.append([])
        for j in range(tam):
            #   Añadir sublistas/columnas en cada fila.
            if i == 0 and j == 0:
                m_inter[i].append("2")

            else:
                m_inter[i].append("0")

    m_final = road_maker(m_inter, tam)

    return m_final, tam

#   Realiza los ciclos hasta terminar el programa
def motor(matriz, tam):
    funciona = True
    contador = 0
    while funciona:
            #  Imprimir(matriz, tam)
            matriz, funciona = wasd_mov(matriz, tam, funciona)
            print("Proyecto 1")
            print("Para moverse en la matriz se usarán las teclas ", end=" ")
            print("WASD como en un shooter")
            imprimir(matriz, tam)
            contador += 1
            if funciona == False:
                print("Fin del juego")
                print("Movimientos realizados:", contador)
                menu()
                break
def menu():

    print("Ha terminado de usar el juego laberinto")
    print("Desea continuar? si = Y/ no = N")
    while True:
        string_val = input("Ingrese su decisión: ")

        if string_val.upper() == "Y":
            matriz = []
            matriz, tam = matriz_inter()
            motor(matriz, tam)
            break
        if string_val.upper() == "N":
            print("Ha finalizado el juego")
            break
        else:
            print("Ha ingresado un valor que no otorga ninguna ", end=" ")
            print("respuesta del programa ¬_¬")
            print("Intente nuevamente")
        continue


if __name__ == '__main__':

    funciona = True
    matriz = []
    matriz, tam = matriz_inter()
    system('clear')

    motor(matriz, tam)
    matriz, tam = menu()
